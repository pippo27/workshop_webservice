//
//  UserViewController.swift
//  workshop_webservice
//
//  Created by Arthit Thongpan on 9/9/2558 BE.
//  Copyright © 2558 Arthit Thongpan. All rights reserved.
//

import UIKit

class UserViewController: UIViewController, UITableViewDataSource, UITableViewDelegate  {

    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var users:[User] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupBarButtonItem()
        setupTitle()
        loadAllData()
    }
    
    
    override func viewWillAppear(animated: Bool) {
        
    }
    
    func setupTitle(){
        self.title = "Users"
    }
    
    func setupBarButtonItem() {
        self.navigationItem.leftBarButtonItem = self.editButtonItem()
        
        
        let addButton = UIBarButtonItem(barButtonSystemItem: .Add, target: self, action: #selector(UserViewController.showTextEntryAlert(_:)))
        self.navigationItem.rightBarButtonItem = addButton

       
    
    }
    
    override func setEditing(editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        tableView!.setEditing(editing, animated: animated)
    }
    
    // MARK: - Table View
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("UserTableViewCellIdentifier", forIndexPath: indexPath)
        
        let user = users[indexPath.row]
        cell.textLabel!.text = "\(user.username) \(user.password)"
        
        return cell
    }
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            removeUser(indexPath)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }
        

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showDetail" {
            let indexPath = self.tableView.indexPathForSelectedRow
            let user = users[indexPath!.row]
            let controller = segue.destinationViewController as! DetailViewController
            controller.user = user
            controller.delegate = self
            controller.navigationItem.leftItemsSupplementBackButton = true
        }
    }
    
    

    // MARK: - Service
    
    func loadAllData() {
        self.users = []
        let service = Service()
        service.loadData(ServiceURL.AllUser, httpMethod: HttpMethod.GET, params: nil) { (data, response, error) -> Void in
            let dataArray:NSArray = data["users"] as! NSArray!
            //Read data
            for dic in dataArray {
                
                let strId = dic[kUserId] as! String
                let userId = Int(strId)
                let username = dic[kUsername] as! String
                let password  = dic[kPassword] as! String
                
                let user = User(userId: userId!, username: username, password: password)
                self.users.append(user) //Add data from database to array
                
            }
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.view.setNeedsLayout()
                self.tableView.reloadData()
            })
            
            
        }
    }
    
    func removeUser(indexPath:NSIndexPath) {
        let user = users[indexPath.row]
        
        let service = Service()
        let url = "\(ServiceURL.RemoveUser)/\(user.userId)"
        service.loadData(url, httpMethod: HttpMethod.DELETE, params: nil) { (data, response, error) -> Void in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.users.removeAtIndex(indexPath.row)
                self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            })
            
        }
    }
    
    func updateUser(user:User, isDataChange:Bool) {
        if !isDataChange {
            return
        }
        
        let indexPath = self.tableView.indexPathForSelectedRow
        let service = Service()
        let param = [
            "id":user.userId,
            "username": user.username,
            "password" : user.password
        ]
        
        let url = "\(ServiceURL.UpdateUser)/\(user.userId)"
        service.loadData(url, httpMethod: HttpMethod.PUT, params: param) { (data, response, error) -> Void in
           
            let username = data[kUsername] as! String
            let password  = data[kPassword] as! String
            
    
            self.users[indexPath!.row].username = username
            self.users[indexPath!.row].password = password

            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.tableView.reloadData()
                //self.tableView.reloadRowsAtIndexPaths([indexPath!], withRowAnimation: UITableViewRowAnimation.Fade)
            })
            
        }
    }
    
    func register(username:String, password:String) {
        let param = [
            "username": username,
            "password" : password
        ]
        
        let service = Service()
        service.loadData(ServiceURL.AddUser, params: param) { (data, response, error) -> Void in
            
            let dic = data["user"] as! [String: AnyObject]
            let strId = dic[kUserId] as! String
            let userId = Int(strId)
            let username = dic[kUsername] as! String
            let password  = dic[kPassword] as! String
            
            let user = User(userId: userId!, username: username, password: password)
            self.users.insert(user, atIndex: 0)
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                let indexPath = NSIndexPath(forRow: 0, inSection: 0)
                self.tableView.insertRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
            })
            
        }

    }
    
    
    // MARK: - Alert Register
    
    func showTextEntryAlert(sender:AnyObject) {
        let title = "Register"
        let message = "กรุณากรอก username และ password"
        let cancelButtonTitle = "Cancel"
        let otherButtonTitle = "OK"
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        
        // Add the text field for text entry.
        alertController.addTextFieldWithConfigurationHandler { textField in
            // If you need to customize the text field, you can do so here.
            textField.placeholder = NSLocalizedString("Username", comment: "")
            //textField.keyboardType = .EmailAddress
        }
        
        
        
        alertController.addTextFieldWithConfigurationHandler { (textField) in
            textField.placeholder = NSLocalizedString("Password", comment: "")
            textField.secureTextEntry = true
        }
        
        
        // Create the actions.
        let cancelAction = UIAlertAction(title: cancelButtonTitle, style: .Cancel) { action in
            //NSLog("The \"Text Entry\" alert's cancel action occured.")
        }
        
        let otherAction = UIAlertAction(title: otherButtonTitle, style: .Default) { action in
            //NSLog("The \"Text Entry\" alert's other action occured.")
            
            let usernameTextField = alertController.textFields![0]
            let passwordTextField = alertController.textFields![1]
            
            let username = usernameTextField.text
            let password = passwordTextField.text
        
            if !username!.isEmpty && !password!.isEmpty{
               
                self.register(username!, password: password!)
            }
            
        }
        
        // Add the actions.
        alertController.addAction(cancelAction)
        alertController.addAction(otherAction)
        
        presentViewController(alertController, animated: true, completion: nil)
    }
    


}

