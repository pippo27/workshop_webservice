//
//  Alert.swift
//  workshop_webservice
//
//  Created by Arthit Thongpan on 10/20/15.
//  Copyright © 2015 Arthit Thongpan. All rights reserved.
//

import UIKit

func Alert(title:String, message:String, buttonTitle:String = "OK", target:AnyObject){
    
    let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
    let cancelAction = UIAlertAction(title: buttonTitle, style: .Cancel) { (action) -> Void in
        
    }
    alertController.addAction(cancelAction)
    target.presentViewController(alertController, animated: true, completion: nil)
}