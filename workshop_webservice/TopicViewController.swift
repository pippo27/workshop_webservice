//
//  TopicViewController.swift
//  workshop_webservice
//
//  Created by Arthit Thongpan on 10/21/15.
//  Copyright © 2015 Arthit Thongpan. All rights reserved.
//

import UIKit
import Alamofire

class TopicViewController: UIViewController, UITableViewDataSource, UITableViewDelegate  {

    
    @IBOutlet weak var tableView: UITableView!
    let topicList = TopicList.shared
    override func viewDidLoad() {
        super.viewDidLoad()

        loadTopic()
        setupTitle()
//        setupBarButtonItem()
    }
    
    func setupTitle(){
        self.title = "กระทู้"
    }
//    
//    func setupBarButtonItem(){
//        let addButton = UIBarButtonItem(barButtonSystemItem: .Add, target: self, action: "onAddButtonClicked:")
//        self.navigationItem.rightBarButtonItem = addButton
//    }
    
    func loadTopic(){
        
        let service = Service()
        service.loadData(ServiceURL.AllTopic, httpMethod: HttpMethod.GET, params: nil) { (data, response, error) -> Void in
            let dataArray:NSArray = data["topics"] as! NSArray!
            //Read data
            for dict in dataArray {
                
                
                let topic = Topic(dict: dict as! [String : AnyObject])
                
                self.topicList.add(topic)
            }
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.view.setNeedsLayout()
                self.tableView.reloadData()
            })
            
        }

        
    }
    
    
    
    // MARK: - Table View
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return topicList.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("TopicTableViewCellIdentifier", forIndexPath: indexPath)
        
       // let user = users[indexPath.row]
        let topic = topicList.topicAtIndex(indexPath.row)
        cell.textLabel!.text =  topic.title
        cell.detailTextLabel!.text = topic.detail
        
        return cell
    }

    //MARK: - Unwind Segue Methods
    
    @IBAction func cancelToTopicViewController(segue: UIStoryboardSegue) {
        
    }
    
    @IBAction func addToTopicViewController(segue: UIStoryboardSegue) {
        if let controller = segue.sourceViewController as? AddTopicViewController, newTopic = controller.newTopic {
            callCreateNewTopic(newTopic)
        }
    }

    func callCreateNewTopic(newTopic:Topic) {
        let parameters:[String : AnyObject!] = [
            "topic": newTopic.title,
            "details": newTopic.detail,
            "userId" : user.userId,
            "view": "0",
            "reply": "0"
        ]
        
        let service = Service()
        service.loadData(ServiceURL.AddTopic, httpMethod: HttpMethod.POST, params: parameters) { (data, response, error) in
            guard let dict = data["topic"] else {
                print("response new topic error")
                return
            }
        
            let topic = Topic(dict: dict as! [String : AnyObject])

            self.topicList.add(topic, atIndex: 0)
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                let indexPath = NSIndexPath(forRow: 0, inSection: 0)
                self.tableView.insertRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
            })
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.view.setNeedsLayout()
                self.tableView.reloadData()
            })

        }
        
    }
}
