//
//  DetailViewController.swift
//  workshop_webservice
//
//  Created by Arthit Thongpan on 9/9/2558 BE.
//  Copyright © 2558 Arthit Thongpan. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!

    var delegate:UserViewController!
    var user: User!
    
    var isDataChange:Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        configEditButton()
        configureView()
    }
    func configEditButton(){
       // let addButton = UIBarButtonItem(barButtonSystemItem: .Edit, target: self, action: "onEditButtonTapped:")
        self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    func configureView() {
        idLabel.text = "ID:\(user.userId)"
        usernameTextField.text  = user.username
        passwordTextField.text  = user.password
    }
    
    override func setEditing(editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        
        usernameTextField.userInteractionEnabled = editing
        passwordTextField.userInteractionEnabled = editing
        
        user.username = usernameTextField.text!
        user.password = passwordTextField.text!
        isDataChange = true
    }
    
    override func willMoveToParentViewController(parent: UIViewController?) {
        super.willMoveToParentViewController(parent)
        //print("willMoveToParentViewController")
        if parent == nil {
            print("Back button pressed")
            
            // it can be useful to store this into a BOOL property
            delegate.updateUser(user, isDataChange: isDataChange)
            
        } else {
            print("Back button not pressed")
            
        }
    }
}
