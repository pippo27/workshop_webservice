//
//  Topic.swift
//  workshop_webservice
//
//  Created by Arthit Thongpan on 10/21/15.
//  Copyright © 2015 Arthit Thongpan. All rights reserved.
//

import UIKit


let kTopicId = "id"
let kTopicTitle = "topic"
let kTopicDetail = "details"
let kTopicCreateByUserId = "userId"
let kTopicViewCount = "view"
let kTopicReplyCount = "reply"
let kTopicCreateAt = "createAt"

class Topic: NSObject {
    var topicId:Int
    var title:String
    var detail:String
    var createByUserId:Int
    var viewCount:Int
    var replyCount:Int
    var createAt:String
    
    convenience init(title:String, detail:String) {
        self.init(topicId: 0, title: title , detail: detail, createByUserId: 0, viewCount: 0,replyCount: 0, createAt: "")
    }
    
    convenience init(dict:[String: AnyObject]) {
        let topicId = Int(dict[kTopicId] as! String)
        let title = dict[kTopicTitle] as! String
        let detail  = dict[kTopicDetail] as! String
        let createByUserId  =  Int(dict[kTopicCreateByUserId] as! String)
        let viewCount = Int(dict[kTopicViewCount] as! String)
        let replyCount = Int(dict[kTopicReplyCount] as! String)
        let createAt = dict[kTopicCreateAt] as! String
        

        self.init(topicId: topicId!, title: title , detail: detail, createByUserId: createByUserId!, viewCount: viewCount!, replyCount: replyCount!, createAt: createAt)
    }
    
    init(topicId:Int, title:String, detail:String, createByUserId:Int, viewCount:Int, replyCount:Int, createAt:String) {
        self.topicId = topicId
        self.title = title
        self.detail = detail
        self.createByUserId = createByUserId
        self.viewCount = viewCount
        self.replyCount = replyCount
        self.createAt = createAt
        
        super.init()
    }
    
}