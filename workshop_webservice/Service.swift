//
//  service.swift
//
//
//  Created by Arthit Thongpan on 2/8/2558 BE.
//  Copyright (c) 2558 Arthit Thongpan. All rights reserved.
//

import UIKit

enum HttpMethod: String {
    case OPTIONS, GET, HEAD, POST, PUT, PATCH, DELETE, TRACE, CONNECT
}

class Service: NSObject {
    
    
    func loadData(url:String, httpMethod:HttpMethod = HttpMethod.POST , params:AnyObject?, completionHandler:(data:NSDictionary, response:NSURLResponse, error:NSError?)->Void){
        
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        let session = NSURLSession.sharedSession()
        request.HTTPMethod = httpMethod.rawValue
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        //Prepare Parameter
        if params != nil {
            do{
                let options = NSJSONWritingOptions()
                request.HTTPBody = try NSJSONSerialization.dataWithJSONObject(params!, options: options)
            }catch let err as NSError{
                print(err.localizedDescription);
            }
        }
        
        let task = session.dataTaskWithRequest(request, completionHandler: { (data, response, error) -> Void in
            if error != nil {
                print(error!.localizedDescription)
                return
            }
            
            do {
                let json: NSDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options:NSJSONReadingOptions()) as! NSDictionary
                print("Response \(url)")
                print(json)
                
                completionHandler(data: json, response: response!, error: error)
                
            }
            catch let err as NSError{
                print(err.localizedDescription);
            }
        })
        
        task.resume()
    }
    

    
}
