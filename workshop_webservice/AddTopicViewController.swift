//
//  AddTopicViewController.swift
//  workshop_webservice
//
//  Created by Arthit Thongpan on 10/21/15.
//  Copyright © 2015 Arthit Thongpan. All rights reserved.
//

import UIKit

class AddTopicViewController: UIViewController {
    
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var detailTextView: UITextView!
    
    var newTopic: Topic? {
        if titleTextField.text!.isEmpty || detailTextView.text!.isEmpty {
            return nil
        }
        
        let title = titleTextField.text
        let detail = detailTextView.text
        return Topic(title: title!, detail: detail)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleTextField.becomeFirstResponder()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        titleTextField.resignFirstResponder()
        detailTextView.resignFirstResponder()
    }
}
