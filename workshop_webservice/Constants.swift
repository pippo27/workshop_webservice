//
//  Constants.swift
//  Wines
//
//  Created by Arthit Thongpan on 2/8/2558 BE.
//  Copyright (c) 2558 Arthit Thongpan. All rights reserved.
//

import Foundation


struct ServiceURL {
    static let RootURL     = "http://localhost/webboard"
    static let AllUser     = RootURL + "/users"   //GET
    static let AddUser     = RootURL + "/users"    //POST
    static let RemoveUser  = RootURL + "/users"    //DELETE
    static let UpdateUser  = RootURL + "/users"    //PUT
    
    static let AddTopic    = RootURL + "/topics"   //POST
    static let AllTopic    = RootURL + "/topics"   //GET
    
    static let AddReply    = RootURL + "/replys"   //POST
    static let GetReplyByTopicID = RootURL + "/replys"   //GET
    
}
