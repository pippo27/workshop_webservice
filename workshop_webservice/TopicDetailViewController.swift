//
//  TopicDetailViewController.swift
//  workshop_webservice
//
//  Created by Arthit Thongpan on 4/14/16.
//  Copyright © 2016 Arthit Thongpan. All rights reserved.
//

import UIKit

class TopicDetailViewController: UIViewController, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    var topic:Topic!
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
}

// MARK: - UITableViewDataSource
extension TopicDetailViewController {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("TopicDetailCell", forIndexPath: indexPath) as! TopicDetailTableViewCell
        cell
        return cell
    }
}
