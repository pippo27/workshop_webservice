//
//  User.swift
//  workshop_webservice
//
//  Created by Arthit Thongpan on 9/9/2558 BE.
//  Copyright © 2558 Arthit Thongpan. All rights reserved.
//

let kUserId = "id"
let kUsername = "username"
let kPassword  = "password"

struct User {
    var userId:Int
    var username:String
    var password:String
    
    init(userId:Int, username:String, password:String) {
        self.userId = userId
        self.username = username
        self.password = password
    }
}