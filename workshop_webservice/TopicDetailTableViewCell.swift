//
//  TopicDetailTableViewCell.swift
//  workshop_webservice
//
//  Created by Arthit Thongpan on 4/14/16.
//  Copyright © 2016 Arthit Thongpan. All rights reserved.
//

import UIKit

class TopicDetailTableViewCell: UITableViewCell {

    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailTextView: UITextView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
