//
//  TopicList.swift
//  workshop_webservice
//
//  Created by Arthit Thongpan on 10/21/15.
//  Copyright © 2015 Arthit Thongpan. All rights reserved.
//

import UIKit

class TopicList: NSObject {
    
    private var topics:[Topic]!
    
    var count:Int{
        get {
            return topics.count;
        }
    }
    
    
    //MARK: - Singleton Pattern
    class var shared : TopicList {
        
        struct Static {
            static let instance:TopicList = TopicList()
        }
        
        return Static.instance
    }
    override private init() {
        super.init()
        topics = []
    }
    func add(topic:Topic){
        topics.append(topic)
        
    }
    
    func add(topic:Topic, atIndex:Int) {
        topics.insert(topic, atIndex: atIndex)
    }
    
    func topicAtIndex(index:Int)->Topic {
        return topics[index]
    }
    
    func remove(topic:Topic){
        for value in topics {
            if let index = topics.indexOf(value) {
                topics.removeAtIndex(index)
                return
            }
        }
    }
    
}
